package kap16;

import java.util.List;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 5 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class SokResultat {

    public static void main(String[] args) {

	Scanner scanner = new Scanner(System.in);

	System.out.print("Ange filnamn : ");
	String filnamn = scanner.nextLine();

	// Try with resources, �ppnar en BufferedReader
	try (BufferedReader reader = new BufferedReader(new FileReader(filnamn))) {

	    // Variabler
	    String line;

	    // Lista f�r alla rader i filen.
	    List<String> items = new ArrayList<String>();

	    // l�ser in varje rad i filen och delar den.
	    while ((line = reader.readLine()) != null) {
		items.add(line);

	    }

	    // Loop
	    while (true) {

		System.out.print("S�k efter ett namn : ");
		String namn = scanner.nextLine();

		// Om Listan inneh�ller ett namn.
		if (items.contains(namn)) {

		    // H�mta po�ngen baserat p� namnets plats i Listan + 1.
		    String poang = items.get(items.indexOf(namn) + 1);
		    // Skriv ut resultatet
		    System.out.println(namn + "\t" + poang);
		    
		// Avsluta med #
		} else if (namn.equalsIgnoreCase("#")) {
		    
		    System.out.println("Avslutar...");
		    
		    // Hoppar ut while loopen
		    break;

		} else {
		    // Hittade inget i Listan
		    System.out.println("Namnet finns inte i listan.");
		}

	    }
	} catch (Exception ex) {
	    
	    ex.printStackTrace();
	}
	// St�nger scanner f�r att slippa memoryleaks.
	scanner.close();

    }

}
