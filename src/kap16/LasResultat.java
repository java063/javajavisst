package kap16;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 5 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class LasResultat {

    public static void main(String[] args) {
	
	Scanner scanner = new Scanner(System.in);

	String namn;
	int tal, antal = 0, summa = 0;

	System.out.print("Ange filnamn : ");
	String filnamn = scanner.nextLine();


	try(BufferedReader reader = new BufferedReader(new FileReader(filnamn))){
	    
	    
	    while(true){
		
		namn = reader.readLine();
		
		if(namn == null)
		    break;
		System.out.print(namn + "\t");
		tal = Integer.parseInt(reader.readLine());
		System.out.println(tal);
		
		summa += tal;
		antal++;

	    }
	}catch(Exception ex){
	    ex.printStackTrace();
	}
	
	System.out.println("\nSumma : " + summa);
	System.out.println("Antal : " + antal );
	System.out.println("Medel : " + ((double) summa/antal));

	scanner.close();

    }

}
