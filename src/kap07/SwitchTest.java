package kap07;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SwitchTest extends JPanel implements ActionListener {

    JLabel lbl;
    JTextField txtField;
    Color color;
    String str;
    char code;

    public SwitchTest() {
	lbl = new JLabel("Skriv R, G eller B?");
	txtField = new JTextField(10);
	this.add(lbl);
	this.add(txtField);

	txtField.addActionListener(this);
    }

    @Override
    public void paintComponent(Graphics graphics) {

	switch (code) {

	case 'R':
	    str = "R�d f�rg";
	    color = new Color(255, 0, 0);
	    break;
	case 'G':
	    str = "Gr�n f�rg";
	    color = new Color(0, 255, 0);
	    break;
	case 'B':
	    str = "Bl� f�rg";
	    color = new Color(0, 0, 255);
	    break;
	default:
	    str = "Vit f�rg";
	    color = new Color(255);
	    break;
	}
	graphics.setColor(color);
	graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
	graphics.setColor(new Color(0, 0, 0));
	graphics.drawString(str, 10, 100);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
	code = txtField.getText().toUpperCase().charAt(0);
	this.repaint();
    }

    public static void main(String[] args) {
	JFrame frame = new JFrame("Switch Test");
	frame.setLocationRelativeTo(null);
	frame.setSize(400, 400);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.add(new SwitchTest());
	frame.setVisible(true);
	
    }

}
