/**
 * 
 */
package kap04.inmatatal;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
@SuppressWarnings("serial")
public class InmataTal extends JFrame implements ActionListener {

    JTextField tf1, tf2;
    JButton btn;
    JLabel lbl1, lbl2;
    int x, y, summa;

    public InmataTal() {

	this.setLayout(new FlowLayout());
	lbl1 = new JLabel("Skriv ett heltal i vardera ruta:");
	this.add(lbl1);

	tf1 = new JTextField(5);
	this.add(tf1);
	tf1.addActionListener(this);

	tf2 = new JTextField(5);
	this.add(tf2);
	tf2.addActionListener(this);

	btn = new JButton("Addera talen!");
	btn.addActionListener(this);
	this.add(btn);

	lbl2 = new JLabel();
	this.add(lbl2);
    }

    /*
     * 
     */
    @Override
    public void actionPerformed(ActionEvent paramActionEvent) {
	try {
	    x = Integer.parseInt(tf1.getText());
	    y = Integer.parseInt(tf2.getText());
	    summa = x + y;
	    lbl2.setText("Summan �r " + summa);
	} catch (Exception ex) {
	    summa = 0;
	    lbl2.setText("Felaktig inmatning");

	}

    }

    /**
     * @param args
     */
    public static void main(String[] args) {
	InmataTal f = new InmataTal();
	f.setSize(200, 200);
	f.setLocation(100, 100);
	f.setTitle("Mata in tal");
	f.setDefaultCloseOperation(EXIT_ON_CLOSE);
	f.setVisible(true);
    }

}
