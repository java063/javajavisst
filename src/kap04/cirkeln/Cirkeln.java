/**
 * 
 */
package kap04.cirkeln;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 1 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
@SuppressWarnings("serial")
public class Cirkeln extends JFrame implements ActionListener {

    JButton btn;
    JTextField tf;
    JLabel lbl_ledtxt, lbl_area, lbl_omkrets;
    double radie, area, omkrets;

    /**
     * 
     */
    public Cirkeln() {
	this.setLayout(new FlowLayout());
	btn = new JButton("OK");
	lbl_ledtxt = new JLabel("Ange radie");
	tf = new JTextField(10);
	lbl_area = new JLabel();
	lbl_omkrets = new JLabel();
	add(lbl_ledtxt);
	add(tf);
	add(btn);
	btn.addActionListener(this);
	tf.addActionListener(this);
	add(lbl_area);
	add(lbl_omkrets);
    }

    /* 
     * 
     */
    @Override
    public void actionPerformed(ActionEvent event) {
	radie = Double.parseDouble(tf.getText());
	area = radie * radie * Math.PI;
	omkrets = 2 * Math.PI * radie;
	lbl_area.setText(String.format("Arean �r %5.3f", area));
	lbl_omkrets.setText(String.format("Omkretsen �r %5.3f  ", omkrets));
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
	Cirkeln f = new Cirkeln();
	f.setSize(200, 200);
	f.setLocation(100, 100);
	f.setTitle("Cirkeln");
	f.setDefaultCloseOperation(EXIT_ON_CLOSE);
	f.setVisible(true);
    }

}
