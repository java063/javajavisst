/**
 * 
 */
package kap09.sortera;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 4 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Sortera {

    /**
     * @param args
     */
    public static void main(String[] args) {
	int n = 100;
	int[] a = new int[n];

	/**
	 * Loopar s� l�nge 'i' �r mindre arrayen 'a' storlek.</br>
	 * Samma princip som ovanst�ende fast p� ett aningen mer 'dynamiskt' s�tt.
	 */
	for (int i = 0; i < a.length; i++) {

	    // Slumpar fram ett tal och lagrar det p� position 'i' i arrayen.
	    a[i] = (int) (1000 * Math.random());

	    // Radbryt var 10:e
	    if (i % 10 == 0) {
		System.out.println("");
	    }
	    // Skriver ut slumpen som sparats i arrayen och l�gger till en tab.
	    System.out.print(a[i] + "\t");

	}

	// Sortering
	int s;
	for (int i = 0; i < n - 1; i++) {

	    for (int j = i + 1; j < n; j++) {

		if (a[i] > a[j]) {

		    s = a[i];
		    a[i] = a[j];
		    a[j] = s;

		}
	    }
	}

	System.out.println("\n\nSorterat....");
	for (int i = 0; i < n; i++) {

	    if (i % 10 == 0) {
		System.out.println("");
	    }
	    System.out.print(a[i] + "\t");
	}
    }

}
