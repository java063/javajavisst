/**
 * 
 */
package kap09.arraytest;

import java.util.Scanner;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 3 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */

public class ArrayTest {

    /**
     * @param args
     */
    public static void main(String[] args) {

	Scanner sc = new Scanner(System.in);

	// En statisk array initieras med 5 poster
	int[] a = new int[5];

	// loopar antalet g�nger arrayen �r stor
	for (int i = 0; i < a.length; i++) {
	    System.out.print("Skriv in tal nummer " + i + ": ");
	    a[i] = sc.nextInt();
	}

	System.out.println("Du matade in dessa tal:");

	// loopar antalet g�nger arrayen �r stor
	for (int i = 0; i < a.length; i++) {
	    System.out.println("Tal nummer " + i + " �r " + a[i]);
	}
	sc.close();
    }

}
