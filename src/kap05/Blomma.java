package kap05;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Blomma extends JPanel {

    public Blomma() {

    }

    public void paintComponent(Graphics graphics) {

	super.paintComponent(graphics);

	graphics.setColor(Color.RED);

	int xPos = 0, yPos = 0;
	int size = 32, index = 1;

	for (int i = 0; i < 3; i++) {
	    for (int j = 0; j < 3; j++) {

		if (index % 2 == 1 && index != 5) {
		    
		    if (index == 1)
			graphics.fillOval(100 + xPos + (size/2), 100 + yPos + (size/2), size / 2, size / 2);
		    if (index == 3)
			graphics.fillOval(100 + xPos, 100 + yPos + (size/2), size / 2, size / 2);
		    if (index == 7)
			graphics.fillOval(100 + xPos + (size/2), 100 + yPos, size / 2, size / 2);
		    if (index == 9)
			graphics.fillOval(100 + xPos, 100 + yPos, size / 2, size / 2);

		} else {
		    graphics.fillOval(100 + xPos, 100 + yPos, size, size);

		}

		if (index == 4) {
		    graphics.setColor(Color.YELLOW);

		} else {
		    graphics.setColor((index % 2 == 0) ? Color.RED : Color.BLUE);

		}
		index++;
		xPos += size;
	    }
	    yPos += size;
	    xPos = 0;

	}
	graphics.setColor(Color.GREEN);
	graphics.fillRect(144, yPos * 2, 8, 128);

	for (int i = 1; i < 3; i++) {
	    if (i % 2 == 1) {
		graphics.fillOval(xPos * i + 112, yPos + size * 4, size, size);
	    } else {
		graphics.fillOval(xPos * i + 150, yPos + size * 4, size, size);
	    }

	}

    }

    public static void main(String[] args) {
	JFrame frame = new JFrame("Blomma");
	frame.setSize(500, 500);
	frame.setLocationRelativeTo(null);

	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	frame.add(new Blomma());
	frame.setVisible(true);

    }

}
