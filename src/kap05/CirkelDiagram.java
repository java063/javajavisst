package kap05;

import java.awt.Color;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class CirkelDiagram extends JPanel {

    public CirkelDiagram() {
    }

    @Override
    public void paintComponent(Graphics graphics) {
	super.paintComponent(graphics);
	graphics.setColor(Color.RED);
	graphics.fillArc(50, 50, 200, 200, 0, 90);
	graphics.setColor(Color.GREEN);
	graphics.fillArc(50, 50, 200, 200, 90, 120);
	graphics.setColor(Color.YELLOW);
	graphics.fillArc(50, 50, 200, 200, 210, 150);

    }

    public static void main(String[] args) {
	JFrame frame = new JFrame("Blomma");
	frame.setSize(500, 500);
	frame.setLocationRelativeTo(null);

	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	frame.add(new CirkelDiagram());
	frame.setVisible(true);

    }

}
