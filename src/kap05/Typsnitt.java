package kap05;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
import java.awt.GraphicsEnvironment;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

public class Typsnitt {

    public static void main(String[] args) {

	GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
	String[] fonts = genv.getAvailableFontFamilyNames();

	try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter("fonts.txt")))) {

	    for (String fnt : fonts) {
		pw.println(fnt);
		System.out.println(fnt);
	    }
	} catch (Exception e) {

	}

    }

}
