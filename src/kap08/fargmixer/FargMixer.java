/**
 * 
 */
package kap08.fargmixer;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 4 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
@SuppressWarnings("serial")
public class FargMixer extends JPanel implements ChangeListener {

    private JSlider sliderR, sliderG, sliderB;

    private JTextField tfR, tfG, tfB;
    int r = 128, g = 128, b = 128;

    public FargMixer() {
	sliderR = new JSlider(0, 0, 255, 128);
	sliderR.setPaintTicks(true);
	sliderR.setMajorTickSpacing(50);
	sliderR.setMinorTickSpacing(10);
	sliderR.setPaintLabels(true);
	sliderR.addChangeListener(this);
	tfR = new JTextField(8);
	tfR.setText("R�tt: " + r);

	this.add(sliderR);
	this.add(tfR);

	sliderG = new JSlider(0, 0, 255, 128);
	sliderG.setPaintTicks(true);
	sliderG.setMajorTickSpacing(50);
	sliderG.setMinorTickSpacing(10);
	sliderG.setPaintLabels(true);
	sliderG.addChangeListener(this);
	tfG = new JTextField(8);
	tfG.setText("Gr�nt: " + g);

	this.add(sliderG);
	this.add(tfG);

	sliderB = new JSlider(0, 0, 255, 128);
	sliderB.setPaintTicks(true);
	sliderB.setMajorTickSpacing(50);
	sliderB.setMinorTickSpacing(10);
	sliderB.setPaintLabels(true);
	sliderB.addChangeListener(this);
	tfB = new JTextField(8);
	tfB.setText("Bl�tt: " + b);

	this.add(sliderB);
	this.add(tfB);

	this.setBackground(new Color(r, g, b));
    }

    /* 
     * 
     */
    @Override
    public void stateChanged(ChangeEvent paramChangeEvent) {
	r = sliderR.getValue();
	tfR.setText("R�tt : " + r);

	g = sliderG.getValue();
	tfG.setText("Gr�nt : " + g);

	b = sliderB.getValue();
	tfB.setText("Bl�tt : " + b);

	this.setBackground(new Color(r, g, b));
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
	JFrame frame = new JFrame();
	frame.setSize(400, 300);
	frame.setLocation(100, 100);
	frame.setTitle("FargMixer");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	FargMixer mixer = new FargMixer();
	frame.add(mixer);
	frame.setVisible(true);
    }

}
