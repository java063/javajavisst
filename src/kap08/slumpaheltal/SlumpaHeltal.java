/**
 * 
 */
package kap08.slumpaheltal;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 1 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class SlumpaHeltal {

    /**
     * @param args
     */
    public static void main(String[] args) {

	// double slump = Math.random();
	// System.out.println("Slumpa tal 0.0 - 0.99999999.... : " + slump);
	//
	// slump = 6 * Math.random();
	// System.out.println("Slumpa tal 0.0 - 5.99999999.... : " + slump);
	//
	// int tal = (int) (6 * Math.random());
	// System.out.println("Slumpa heltal 0 - 5.... : " + tal);
	//
	// tal = 1 + (int) (6 * Math.random());
	// System.out.println("Slumpa heltal 1 - 6.... : " + tal);

	int t;
	for (int i = 1; i <= 12; i++) {
	    t = (int) (256 * Math.random());
	    System.out.println("Slumpat heltal nummer " + i + ": " + t);
	}
    }

}
