/**
 * 
 */
package kap08.joptest;

import javax.swing.JOptionPane;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 1 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class JOPtest {

    /**
     * @param args
     */
    public static void main(String[] args) {

	String ort = JOptionPane.showInputDialog("Vad heter orten?");

	char bokst = JOptionPane.showInputDialog("L�nsbokstav?").charAt(0);

	int inv = Integer.parseInt(JOptionPane.showInputDialog("Folkm�ngd?"));

	double avst = Double.parseDouble(JOptionPane.showInputDialog("Avst�nd?"));
	JOptionPane.showMessageDialog(null, ort + " ligger i " + bokst + "-l�n\n" + ort + " har " + inv + " inv�nare\n"
		+ "Avst�nd till " + ort + ": " + avst + " mil");

	System.out.println(ort + " ligger i " + bokst + "-l�n");
	System.out.println(ort + " har " + inv + " inv�nare");
	System.out.println("Avst�nd till " + ort + ": " + avst + " mil");
    }

}
