/**
 * 
 */
package kap08.slumpafarg;

import java.awt.Button;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 1 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
@SuppressWarnings("serial")
public class SlumpaFarg extends JPanel implements ActionListener {

    int r, g, b;
    Button btn;

    /**
     * 
     */
    public SlumpaFarg() {
	btn = new Button("Ny F�rg");
	btn.addActionListener(this);
	this.add(btn);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
	JFrame frame = new JFrame("Byta F�rg");
	frame.setSize(300, 400);
	frame.setLocation(100, 100);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	SlumpaFarg slf = new SlumpaFarg();
	frame.add(slf);
	frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
	r = (int) (256 * Math.random());
	g = (int) (256 * Math.random());
	b = (int) (256 * Math.random());
	this.setBackground(new Color(r, g, b));
	System.out.println("new Color(" + r + ", " + g + ", " + b + ");");

    }

}
