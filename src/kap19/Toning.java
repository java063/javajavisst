package kap19;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 5 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Toning extends JPanel {

    public Toning() {

    }

    public void paintComponent(Graphics graphics) {

	super.paintComponent(graphics);
	Graphics2D graphics2D = (Graphics2D) graphics;

	// alt. setColor(new Color(0, 0, 255))
	// graphics2D.setColor(Color.BLUE);
	GradientPaint gradient = new GradientPaint(200, 50, Color.black, 50, 100, Color.white, true);
	graphics2D.setPaint(gradient);
	// x, y, width, height
	graphics2D.fillRect(20, 20, 250, 250);

    }

    public static void main(String[] args) {
	JFrame frame = new JFrame();

	// width, height
	frame.setSize(400, 400);

	// x, y
	frame.setLocationRelativeTo(null);
	frame.setTitle("Grafik2DTest");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.add(new Toning());
	frame.setVisible(true);

    }

}
