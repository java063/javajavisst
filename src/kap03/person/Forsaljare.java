/**
 * 
 */
package kap03.person;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Forsaljare extends Anstalld {

    private int provision;

    /**
     * 
     */
    public Forsaljare() {
	super();
	this.provision = 4;
    }

    /**
     * @param namn
     * @param ort
     * @param lon
     */
    public Forsaljare(String namn, String ort, int lon, int provision) {
	super(namn, ort, lon);
	this.provision = provision;
    }

    public int getProvision() {
	return provision;
    }

    public void setProvision(int provision) {
	this.provision = provision;
    }

    public String toString() {
	return "-- " + this.getClass().getSimpleName() + " --\nNamn: " + namn + "\nOrt: " + ort + "\nL�n: " + lon
		+ "\nProvision : " + provision + "\n";
    }

}
