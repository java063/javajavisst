/**
 * 
 */
package kap03.person;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Person {

    protected String namn;
    protected String ort;

    public Person() {

    }

    public Person(String namn, String ort) {
	this.namn = namn;
	this.ort = ort;
    }

    public String toString() {
	return "-- " + this.getClass().getSimpleName() + " --\nNamn: " + namn + "\nOrt: " + ort + "\r";
    }

}
