/**
 * 
 */
package kap03.kombibilar;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class KCombi extends KPersonbil {

    /**
     * 
     */
    public KCombi() {
	marke = "Volvo V70";
	arsmodell = 2000;
	nypris = 200000;
	storlek = 3;
    }

    /**
     * @param marke
     * @param arsmodell
     * @param nypris
     * @param storlek
     */
    public KCombi(String marke, int arsmodell, int nypris, int storlek) {
	super(marke, arsmodell, nypris, storlek);
    }

    public void visa() {
	System.out.print("    ");
	int i = 0;
	while (i < storlek) {
	    System.out.print("_");
	    i++;
	}
	System.out.println("_");
	System.out.print(" __/");
	i = 0;
	while (i < storlek) {
	    System.out.print("_");
	    i++;
	}
	System.out.println("_\\");
	System.out.print(" -O-");
	i = 0;
	while (i < storlek) {
	    System.out.print("-");
	    i++;
	}
	System.out.println("O- ");
    }

}
