/**
 * 
 */
package kap03.kombibilar;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class SeKombi {

    /**
     * @param args
     */
    public static void main(String[] args) {
	KCombi p3, p4;

	p3 = new KCombi("VW Golf", 2005, 160000, 2);
	p4 = new KCombi();

	p3.visa();
	System.out.println(p3.fakta());
	System.out.println("Pris idag: " + p3.varde(2010) + "kr");

	p4.visa();
	System.out.println(p4.fakta());
	System.out.println("Pris idag: " + p4.varde(2010) + "kr");

    }

}
