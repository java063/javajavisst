/**
 * 
 */
package kap03.arv;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */

public class Golfboll extends Boll {

    private String fabrikat;

    /**
     * 
     */
    public Golfboll() {
	super();
    }

    /**
     * @param diameter
     * @param material
     * @param farg
     */
    public Golfboll(int diameter, String material, String farg) {
	super(diameter, material, farg);

    }

    public Golfboll(int diameter, String material, String farg, String fabrikat) {
	super(diameter, material, farg);
	this.fabrikat = fabrikat;

    }

    /**
     * Denna metod h�mtar fabrikatet f�r golfbollen.
     * 
     * @return
     */
    public String getFabrikat() {
	return fabrikat;
    }

    /**
     * Denna metod s�tter fabrikatet p� golfbollen
     * 
     * @param fabrikat
     */
    public void setFabrikat(String fabrikat) {
	this.fabrikat = fabrikat;
    }

    /**
     * @ Denna metod rullar bollen tills v�rdet 'i' �verstiger v�rdet n.
     */
    public void rulla(int n) {
	int i = 0;
	while (i < n) {
	    System.out.print("O");
	    i++;
	}
	System.out.println();
    }

    /**
     * Denna metod kollar om golfbollens diameter �r 43, om det st�mmer �r den godk�nd annars underk�nd.
     */
    public String godkand() {
	if (diameter == 43)
	    return "Godk�nd";
	else
	    return "Underk�nd";
    }

}
