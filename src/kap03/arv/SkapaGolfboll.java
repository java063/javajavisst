/**
 * 
 */
package kap03.arv;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */

public class SkapaGolfboll {

    /**
     * @param args
     */
    public static void main(String[] args) {

	Golfboll gb, gb2;

	gb = new Golfboll(43, "Gummi", "Vit", "Titleist");
	gb.rulla(10);
	System.out.println("Volym: " + gb.volym());
	System.out.println(gb.godkand());

	gb2 = new Golfboll(44, "Gummi", "Vit", "Titleist");
	gb2.rulla(10);
	System.out.println("Volym: " + gb2.volym());
	System.out.println(gb2.godkand());
    }

}
