/**
 * 
 */
package kap03.jframe;

import java.awt.HeadlessException;

import javax.swing.JFrame;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */

@SuppressWarnings("serial")
public class MinJFrame extends JFrame {

    /**
     * @throws HeadlessException
     */
    public MinJFrame() {

    }

}
