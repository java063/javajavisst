package extra;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Capitalize {

    private Capitalize() {
    }
    /**
	 * Capitalizes the first character in the string provided as argument and returns it.
	 * @param s
	 * @return
	 */
	public static String toCap(String s) {
		char[] chars = s.toCharArray();
		chars[0] = Character.toUpperCase(chars[0]);
		return new String(chars);
	}

	/**
	 * Capitalize a given character at idx provided in the string.
	 * @param s
	 * @param idx
	 * @return
	 */
	public static String toCap(String s, int idx) {
		char[] chars = s.toCharArray();
		chars[idx] = Character.toUpperCase(chars[idx]);
		return new String(chars);
	}

}
