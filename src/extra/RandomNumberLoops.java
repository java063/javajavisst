package extra;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class RandomNumberLoops {

    public static void main(String[] args) {
	System.out.println("=============== FOR LOOP ===============\n");

	for (int i = 0; i <= 20; i++) {

	    int number = (int) (Math.random() * 1001);

	    if (number % 2 == 0)
		System.out.println("J�mnt nummer : " + number);
	    if (number % 2 == 1)
		System.out.println("Oj�mnt nummer : " + number);

	}

	System.out.println("\n=============== WHILE LOOP ===============\n");

	int i = 0;

	while (i <= 20) {
	    int number = (int) (Math.random() * 1001);

	    if (number % 2 == 0)
		System.out.println("J�mnt nummer : " + number);
	    if (number % 2 == 1)
		System.out.println("Oj�mnt nummer : " + number);
	    i++;
	}

	System.out.println("\n=============== DO/WHILE ===============\n");

	do {

	    int number = (int) (Math.random() * 1001);

	    if (number % 2 == 0)
		System.out.println("J�mnt nummer : " + number);
	    if (number % 2 == 1)
		System.out.println("Oj�mnt nummer : " + number);
	    i--;

	} while (i >= 0);

    }

}
