package extra;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
import java.util.regex.Pattern;

/**
 * Class for splitting strings by \\s+
 * 
 * @author lano78
 * @version 1.0
 */
public class Split {

    // Compiled regesx pattern
    private Pattern compiled = Pattern.compile("[\\s+]");

    /**
     * Default Constructor
     */
    public Split() {

    }

    /**
     * This method split a string by any whitespaces and adds the contents<br>
     * except empty strings to an array and returns it.
     * 
     * @param str
     *            the string to be splitted
     * @return contents as String[]
     */
    public String[] splitStringToWordsArray(String str) {

	String[] words = null;

	try {

	    while (true) {

		words = str.split(this.compiled.pattern());

		for (String s : words) {

		    if (!s.equalsIgnoreCase("")) {
			// System.out.println(s);
		    }
		}

		break;
	    }

	    return words;

	} catch (Exception ex) {

	    ex.printStackTrace();
	}
	return null;
    }

    /**
     * This method splits a string by the rules defined by the regex provided as argument.
     * 
     * @param str
     *            the string to be splitted
     * @param regex
     *            pattern as String
     * @return contents as String[]
     */
    public String[] splitStringToWordsArray(String str, String regex) {

	Pattern compiled = Pattern.compile(regex);

	String[] words = null;

	try {

	    while (true) {

		words = str.split(compiled.pattern());

		for (String s : words) {

		    if (!s.equalsIgnoreCase("")) {
			// System.out.println(s);
		    }
		}

		break;
	    }

	    return words;

	} catch (Exception ex) {

	    ex.printStackTrace();
	}

	return null;
    }

}
