package extra;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
import helpers.OddEven;

public class LoopWithHelperClass {

    public static void main(String[] args) {
	// Antalet rundor
	int round = 0;

	// K�r loopen i tv� rundor
	while (round <= 2) {

	    // �ka antalet rundor med 1.
	    round++;

	    // Loopa igenom 0-20, b�rja p� 20 och r�kna ner till 0.
	    for (int i = 20; i >= 0; i--) {

		// Anv�nd hj�lpklassens metod, kolla om det �r ett j�mn tal.
		// Detta k�rs p� f�rsta rundan eftersom vi kollar om rundan �r 1.
		if (OddEven.isOddOrEven(i) && round == 1)
		    System.out.println("J�mnt nummer " + i);

		// H�r kollar vi efter oj�mna tal
		if (!OddEven.isOddOrEven(i) && round == 2)
		    System.out.println("Oj�mnt nummer " + i);

	    }

	}

    }

}
