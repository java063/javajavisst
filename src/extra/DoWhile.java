package extra;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class DoWhile {

    public static void main(String[] args) {
	int round = 0;
	
	do {
	    
	    round++;
	    
	    for (int i = 0; i <= 20; i++) {
		
		if (i % 2 == 0 && round == 1)
		    System.out.println("J�mnt nummer : " + i);
		
		if(i % 2 == 1 && round == 2)
		    System.out.println("Oj�mnt nummer : " + i);
		
		
	    }
	    
	} while (round <= 2);

    }

}
