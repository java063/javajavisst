package extra.graphics;

import javax.swing.JFrame;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 5 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class DrawCircleTest {

    
    public static void main(String[] args) {
	JFrame frame = new JFrame();
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setContentPane(new Circle(75, 250, 250, 5, true));
	frame.setSize(500, 500);
	frame.setLocationRelativeTo(null);
	frame.setTitle("Circle Test");
	frame.setVisible(true);
	
	System.out.println(frame.getWidth()/2);
    }

}
