package extra;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
import java.util.Scanner;

public class CharScramble {

    public CharScramble() {
	Scanner scanner = new Scanner(System.in);
	
	System.out.print("Skriv ett namn eller mening : ");
	String name = scanner.nextLine().toLowerCase();
	
	// G�r varannan bokstav till uppercase.
	for (int i = 0; i < name.length(); i++) {
	    if(i % 2 == 0){
		name = Capitalize.toCap(name, i);
	    }
	    
	    
	}
	System.out.println(name);
    }

    public static void main(String[] s) {
	new CharScramble();

    }

}
