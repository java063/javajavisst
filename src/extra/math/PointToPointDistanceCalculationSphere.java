package extra.math;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class PointToPointDistanceCalculationSphere {
    
    /**
     * This method calculcate the distance between to points on a sphear.
     * 
     * @param lat1
     * @param long1
     * @param lat2
     * @param long2
     * @param radius
     * @return
     */
    public static double distanceOnSphere(double lat1, double long1, double lat2, double long2, double radius) {
	return radius * (2 * Math.asin(Math.sqrt(Math.pow(Math.sin(Math.toRadians(lat2 - lat1) / 2), 2)
		+ Math.pow(Math.sin(Math.toRadians(long2 - long1) / 2), 2) * Math.cos(lat1) * Math.cos(lat2))));
    }
    

}
