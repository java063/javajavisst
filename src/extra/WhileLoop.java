package extra;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class WhileLoop {

    public static void main(String[] args) {
	int i = 20;
	
	while(i >= 0){	    
	    System.out.println((i%2 == 0)? "J�mnt nummer : " + i : "Oj�mnt nummer : " + i);
	    i--;
	    
	}

    }

}
