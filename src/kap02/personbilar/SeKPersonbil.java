/**
 * 
 */
package kap02.personbilar;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class SeKPersonbil {

    /**
     * @param args
     */
    public static void main(String[] args) {
	KPersonbil b = new KPersonbil();
	b.visa();

    }

}
