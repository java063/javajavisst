/**
 * 
 */
package kap02.matte;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */

public class TestaMatte {

    /**
     * @param args
     */
    public static void main(String[] args) {
	System.out.println("5 i kvadrat ar: " + Matte.sqr(5));
	System.out.println("Talet pi = " + Matte.PI);

    }

}
